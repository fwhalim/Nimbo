//
//  CloudModel.swift
//  Nimbo
//
//  Created by Fanny Halim on 19/07/21.
//

import Foundation

struct CloudModel: Codable, Identifiable {
    
    var id = UUID()
    var thought: String
    var feeling: String
    
}
