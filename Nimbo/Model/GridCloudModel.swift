//
//  GridCloudModel.swift
//  Nimbo
//

//  Created by Fanny Halim on 21/07/21.
//

import Foundation
import SwiftUI

struct CloudItemModel: Identifiable {
    
    let id = UUID()
    let cloud: CloudEntity
    let height: CGFloat
    
}

struct CloudColumnModel: Identifiable {
    
    let id = UUID()
    let cloudItems: [CloudItemModel]
    
}
