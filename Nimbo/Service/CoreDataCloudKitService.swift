//
//  CoreDataService.swift
//  Nimbo
//
//  Created by Fanny Halim on 31/07/21.
//

import Foundation
import CoreData

protocol DataService {
    static var shared: DataService { get }
    var viewContext: NSManagedObjectContext { get }
    func getAll<T: NSManagedObject>(entityType: T.Type) throws -> [T]
    func getById<T: NSManagedObject>(id: NSManagedObjectID) throws -> T
    func delete<T: NSManagedObject>(object: T) throws
    func save() throws
}

class CoreDataCloudKitService: DataService {
    
    static var shared: DataService = CoreDataCloudKitService()
    let persistentContainer: NSPersistentContainer
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    init(inMemory: Bool = false) {
        persistentContainer = NSPersistentCloudKitContainer(name: "CloudContainer")
        
        if inMemory {
            persistentContainer.persistentStoreDescriptions.first?.url = URL(fileURLWithPath: "/dev/null")
        }
        
        persistentContainer.loadPersistentStores { _, err in
            if let err = err {
                print(CoreDataError.failedToInitialize(error: err.localizedDescription))
            }
        }
        
        persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
        persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    }
    
    func getAll<T: NSManagedObject>(entityType: T.Type) throws -> [T] {
        let request = T.fetchRequest()
        do {
            return try viewContext.fetch(request) as? [T] ?? [T]()
        } catch {
            throw CoreDataError.failedToFetch(from: "\(T.self)", error: error.localizedDescription)
        }
    }
    
    func getById<T: NSManagedObject>(id: NSManagedObjectID) throws -> T {
        do {
            let obj = try viewContext.existingObject(with: id)
                
            if let entity = obj as? T {
                return entity
            } else {
                return T()
            }
        } catch {
            throw CoreDataError.failedToFind(error: error.localizedDescription)
        }
    }
    
    func delete<T: NSManagedObject>(object: T) throws {
        viewContext.delete(object)
        try save()
    }
    
    func save() throws {
        do {
            try viewContext.save()
        } catch {
            viewContext.rollback()
            throw CoreDataError.failedToSave(error: error.localizedDescription)
        }
    }
    
    enum CoreDataError: LocalizedError {
        case failedToInitialize(error: String)
        case failedToFetch(from: String, error: String)
        case failedToFind(error: String)
        case failedToSave(error: String)
        
        var errorDescription: String? {
            switch self {
            case .failedToInitialize(error: let error):
                return "Unable to initialize Core Data Stack: \(error)"
            case .failedToFetch(from: let entity, error: let error):
                return "Couldn't fetch data from \(entity): \(error)"
            case .failedToFind(error: let error):
                return "Couldn't find the object: \(error)"
            case .failedToSave(error: let error):
                return "Couldn't save: \(error)"
            }
        }
    }
}
