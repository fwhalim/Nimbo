//
//  JSONFileService.swift
//  Nimbo
//
//  Created by Fanny Halim on 20/07/21.
//

import Foundation

protocol JSONFileServiceDelegate: AnyObject {
    func readJSONData<T: Codable>(filename: String) throws -> T
    func writeJSONData<T: Codable>(filename: String, data: [T]) throws
}

class JSONFileService: JSONFileServiceDelegate {
    
    func readJSONData<T: Codable>(filename: String) throws -> T {
        var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        fileURL = fileURL.appendingPathComponent("\(filename).json")
        let data: Data
        do {
            if !FileManager.default.fileExists(atPath: fileURL.path) {
                try writeJSONData(filename: filename, data: [T]())
            }
            
            data = try Data(contentsOf: fileURL)
        } catch {
            throw JSONError.failedToLoad(from: fileURL.path, error: error.localizedDescription)
        }
        
        do {
            return try JSONDecoder().decode(T.self, from: data)
        } catch {
            throw JSONError.failedToParse(to: "\(T.self)", error: error.localizedDescription)
        }
    }
    
    func writeJSONData<T: Codable>(filename: String, data: [T]) throws {
        var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        fileURL = fileURL.appendingPathComponent("\(filename).json")
        
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            
            try JSONEncoder().encode(data).write(to: fileURL)
            
        } catch {
            throw JSONError.failedToWrite(to: fileURL.path, error: error.localizedDescription)
        }
    }
    
    enum JSONError: LocalizedError {
        case failedToLoad(from: String, error: String)
        case failedToParse(to: String, error: String)
        case failedToWrite(to: String, error: String)
        
        var errorDescription: String? {
            switch self {
            case .failedToLoad(from: let fileURL, error: let error):
                return "Couldn't load from \(fileURL): \(error)"
            case .failedToParse(to: let modelType, error: let error):
                return "Couldn't parse data to \(modelType): \(error)"
            case .failedToWrite(to: let fileURL, error: let error):
                return "Couldn't write data to \(fileURL): \(error)"
            }
        }
    }
}
