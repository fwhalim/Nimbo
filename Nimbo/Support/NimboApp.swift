//
//  NimboApp.swift
//  Nimbo
//
//  Created by Fanny Halim on 28/07/21.
//

import SwiftUI

@main
struct NimboApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
