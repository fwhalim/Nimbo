//
//  BillowView.swift
//  Nimbo
//
//  Created by Fanny Halim on 20/07/21.
//

import SwiftUI

struct BillowView: View {
    
    @StateObject var viewModel = BillowViewModel()
    @State private var showAlert: Bool = false
    @State private var alertMsg: String = ""
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    HStack(alignment: .top, spacing: 10) {
                        ForEach(viewModel.columns) { column in
                            LazyVStack(spacing: 10) {
                                ForEach(column.cloudItems) { item in
                                    Circle()
                                        .foregroundColor(.orange)
                                        .frame(width: item.height, height: item.height)
                                        .overlay(Text(item.cloud.feeling ?? ""))
                                }
                            }
                            Spacer()
                        }
                    }.padding(.horizontal, 10)
                    Spacer()
                }
            }
            .onAppear(perform: {
                do {
                    try viewModel.setupCloudColumns()
                } catch {
                    alertMsg = error.localizedDescription
                    showAlert.toggle()
                }
            })
            .alert(isPresented: $showAlert) {
                Alert(title: Text(alertMsg))
            }
            .navigationTitle("Billow")
        }
    }
}

struct BillowView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModel = BillowViewModel()
        viewModel.columns = []
        
        return BillowView(viewModel: viewModel)
    }
}
