//
//  CloudView.swift
//  Nimbo
//
//  Created by Fanny Halim on 19/07/21.
//

import SwiftUI

struct CloudView: View {
    
    @StateObject var viewModel = CloudViewModel()
    @State private var showAlert: Bool = false
    @State private var alertMsg: String = ""
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextEditor(text: $viewModel.thought)
                        .frame(minHeight: 250)
                        .background(
                            VStack {
                                HStack {
                                    if viewModel.thought.isEmpty {
                                        Text("what's in your mind ?")
                                            .foregroundColor(Color(#colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)))
                                    }
                                    Spacer()
                                }.padding(.horizontal, 4)
                                Spacer()
                            }.padding(.vertical, 8)
                        )
                }
                
                Section {
                    TextField("how do you feel?", text: $viewModel.feeling)
                }
                
                Section {
                    Button(action: {
                        do {
                            try viewModel.validateThought()
                            try viewModel.validateFeel()
                            try viewModel.save()
                            
                            hideKeyboard()
                        } catch {
                            alertMsg = error.localizedDescription
                            showAlert.toggle()
                        }
                    }, label: {
                        Image(systemName: "paperplane.fill")
                    })
                    .frame(maxWidth: .infinity, alignment: .center)
                    .accentColor(.black)
                    .alert(isPresented: $showAlert) {
                        Alert(title: Text(alertMsg))
                    }
                }
            }
            .navigationBarTitle("Hello there...")
        }
    }
}

struct ThoughtView_Previews: PreviewProvider {
    static var previews: some View {
        CloudView()
    }
}
