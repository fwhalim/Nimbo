//
//  ContentView.swift
//  Nimbo
//
//  Created by Fanny Halim on 19/07/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            CloudView()
                .tabItem {
                    Image(systemName: "cloud")
                }
            BillowView()
                .tabItem {
                    Image(systemName: "wind")
                }
        }.accentColor(.black)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
