//
//  BillowViewModel.swift
//  Nimbo
//
//  Created by Fanny Halim on 20/07/21.
//

import Foundation
import SwiftUI

class BillowViewModel: ObservableObject {
    
    @Published var columns = [CloudColumnModel]()
    private var clouds: [CloudEntity] = []
    private var numberOfColumn: Int = 1
    
    private func getAllClouds() throws -> [CloudEntity] {
        return try CoreDataCloudKitService.shared.getAll(entityType: CloudEntity.self)
    }
    
    private func getCloudsInColumn (numberOfColumn: Int, clouds: [CloudEntity]) -> CloudColumnModel {
        let horizontalPadding = CGFloat(20)
        let spacing = CGFloat(numberOfColumn) * 10.0
        let width = (UIScreen.main.bounds.size.width - horizontalPadding - spacing)/2
        let temp =  clouds.map { (cloud) in
            return CloudItemModel(cloud: cloud, height: CGFloat.random(in: 50..<width))
        }
        
        return CloudColumnModel(cloudItems: temp)
    }
    
    func setupCloudColumns() throws {
        clouds = try getAllClouds()
        numberOfColumn = Int.random(in: 2...5)
        var tempColumns = [CloudColumnModel]()
        var tempClouds = [CloudEntity]()
        var isLastColumn = false
        
        if clouds.count >= numberOfColumn, numberOfColumn > 1 {
            let modBy = clouds.count % numberOfColumn
            var divBy = clouds.count / numberOfColumn
            for cloud in clouds {
                tempClouds.append(cloud)
                
                if isLastColumn == false, tempColumns.count == numberOfColumn-1 {
                    divBy += modBy
                    isLastColumn = true
                }
                
                if tempClouds.count == divBy {
                    tempColumns.append(getCloudsInColumn(numberOfColumn: numberOfColumn, clouds: tempClouds))
                    tempClouds.removeAll()
                }
            }
        } else {
            tempColumns = [getCloudsInColumn(numberOfColumn: numberOfColumn, clouds: clouds)]
        }
        
        self.columns = tempColumns
    }
}
