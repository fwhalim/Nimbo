//
//  CloudViewModel.swift
//  Nimbo
//
//  Created by Fanny Halim on 20/07/21.
//

import Foundation

class CloudViewModel: ObservableObject {
        
        @Published var thought: String = ""
        @Published var feeling: String = ""
        
        func validateThought() throws {
            guard thought.count != 0 else { throw CloudInputError.noThought }
            guard thought.count > 10 else { throw CloudInputError.littleThought }
        }
        
        func validateFeel() throws {
            guard feeling.count != 0 else { throw CloudInputError.noFeeling }
            guard feeling.count >= 3 else { throw CloudInputError.unidentifiedFeeling }
            guard feeling.count < 15 else { throw CloudInputError.tooMuchFeeling }
        }
        
    func save(container: DataService = CoreDataCloudKitService.shared) throws {
            let cloud = CloudEntity(context: container.viewContext)
            cloud.thought = thought
            cloud.feeling = feeling
            
            try container.save()
            
            thought = ""
            feeling = ""
        }
    
    enum CloudInputError: LocalizedError {
        case noThought
        case littleThought
        case noFeeling
        case unidentifiedFeeling
        case tooMuchFeeling
        
        var errorDescription: String? {
            switch self {
            case .noThought:
                return "Ask question, even to yourself"
            case .littleThought:
                return "Tell me a little bit more"
            case .noFeeling:
                return "No feeling is a feeling too"
            case .unidentifiedFeeling:
                return "Everything has a name"
            case .tooMuchFeeling:
                return "Which is it?"
            }
        }
    }
}
