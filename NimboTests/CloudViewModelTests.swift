//
//  CloudViewModelTests.swift
//  CloudViewModelTests
//
//  Created by Fanny Halim on 30/08/21.
//

import XCTest
@testable import Nimbo

class CloudViewModelTests: XCTestCase {
    
    typealias ValidationError = CloudViewModel.CloudInputError
    var sut: CloudViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = CloudViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_validateThought() {
        sut.thought = ""
        XCTAssertThrowsError(try sut.validateThought(), "should throw noThought error") { errThrown in
            XCTAssertEqual(errThrown as? ValidationError, ValidationError.noThought)
        }
        
        sut.thought = "just good"
        XCTAssertThrowsError(try sut.validateThought(), "should throw littleThought error") { errThrown in
            XCTAssertEqual(errThrown as? ValidationError, ValidationError.littleThought)
        }
    }
    
    func test_validateFeel() {
        sut.feeling = ""
        XCTAssertThrowsError(try sut.validateFeel(), "should throw noFeeling error") { errThrown in
            XCTAssertEqual(errThrown as? ValidationError, ValidationError.noFeeling)
        }
        
        sut.feeling = "hi"
        XCTAssertThrowsError(try sut.validateFeel(), "should throw unidentifiedFeeling error") { errThrown in
            XCTAssertEqual(errThrown as? ValidationError, ValidationError.unidentifiedFeeling)
        }
        
        sut.feeling = "everyday drink water, everyday inhale-exhale, move here and there"
        XCTAssertThrowsError(try sut.validateFeel(), "should throw tooMuchFeeling") { errThrown in
            XCTAssertEqual(errThrown as? ValidationError, ValidationError.tooMuchFeeling)
        }
    }
    
    func test_saveCloudToCoreData() {
        sut.thought = "And you know one of these days, when I get my money right, buy you everything."
        sut.feeling = "Marry"
        let mockCoreDataService = CoreDataCloudKitService.init(inMemory: true)
        
        try? sut.save(container: mockCoreDataService)
        let clouds = try? mockCoreDataService.getAll(entityType: CloudEntity.self)
        
        XCTAssertNoThrow(clouds)
        XCTAssertEqual(1, clouds?.count, "Should has 1 CloudEntity record")
//        XCTAssertEqual(clouds?.first?.thought, sut.thought, "Should has same thought")
//        XCTAssertEqual(clouds?.first?.feeling, sut.feeling, "Should has same feeling")
    }

}
